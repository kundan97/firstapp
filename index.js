const express = require("express");
const useragent = require("express-useragent");

require("dotenv").config({path: __dirname+'/.env'});

const connectDB = require("./config/db");

let app = express();
app.use(useragent.express());

app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Define Routes
app.use("/", require("./routes/index"));
app.use("/api/url", require("./routes/url"));

const PORT = 5000;
connectDB()
	.then(() => {
		app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
	})
	.catch((err) => console.log(err));
