const mongoose = require("mongoose");
const VisitorInformationSchema = new mongoose.Schema({
    
    urlId: { type: mongoose.Schema.Types.ObjectId, ref: 'Url' },
    isAuthoritative: {type: String},
    isMobile: {type: String},
    isMobileNative: {type: String},
    isTablet: {type: String},
    isiPad: {type: String},
    browser: {type: String},
    version: {type: String},
    os: {type: String},
    platform: {type: String},
    geoIp: {type: String},
    source: {type: String},
    isBot: {type: String},
    isMac: {type: String},
    isChromeOS: {type: String},
    isDesktop: {type: String},
    isWindows: {type: String},
    isLinux: {type: String},
    isLinux64: {type: String},
    status: {type: String},
    country: {type: String},
    countryCode: {type: String},
    state: {type: String},
    city: {type: String},
    udt: { type: Date, default: new Date()},
    cdt: {type: Date, default: new Date()},

});

module.exports = VisitorInformation = mongoose.model("VisitorInformation", VisitorInformationSchema);
