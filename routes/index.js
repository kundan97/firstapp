const express = require('express');
const router = express.Router();
const Url = require('../models/Url');
const VisitorInformation = require('../models/VisitorInformation');
const requestIp = require('request-ip');
const unirest = require("unirest");
const axios = require('axios');

router.post("/getShortUrlList", async (req, res) => {

      const urlData = await Url.find()
                            .select("_id longUrl shortUrl urlCode")
                            .exec();

      res.status(200).json({

          status: "success",
          message: "Retrieve url successfully..",
          data: urlData
      });

});


router.post("/getShortUrlList/visitorInformation", async (req, res) => {

      const { UrlId } = req.body;

      const urlData = await VisitorInformation.find({urlId:UrlId})
                            //.populate("urlId")
                            .exec();

      res.status(200).json({
          status: "success",
          message: "Retrieve short url visitor information successfully..",
          totalClick: urlData.length,
          //data: urlData
      });
});

router.post("/getShortUrlList/byOrigin", async (req, res) => {

      const { country } = req.body;

      const urlData = await VisitorInformation.find({country:"India"})
                            //.populate("urlId")
                            .exec();

      res.status(200).json({
          status: "success",
          message: "Retrieve short url by country successfully..",
          totalClick: urlData.length,
          //data: urlData
      });
});



// @route     GET /:code
// @desc      Redirect to long/original URL
router.get('/:code', async (req, res) => {
  try {
  
    const url = await Url.findOne({ urlCode: req.params.code });
    const useragent = req.useragent;
    //useragent. <variable> of json to extract
    console.log("Lets Play: ", useragent.isDesktop);

    if (url) {

      const clientIp = requestIp.getClientIp(req);

      //http://ip-api.com/json/
      //http://freegeoip.net/json/
      //https://api.ip2location.com/v2/?ip=clientIp&key={YOUR_API_KEY}

      //const staticIp = "2402:3a80:15f2:3397:741e:aedd:edeb:5627";

      let getUserDetails = 'https://www.iplocate.io/api/lookup/'+clientIp;
      const responseData = await axios.get(getUserDetails);

      if(responseData){
        var country = responseData.data.country;
        var countryCode =  responseData.data.country_code;
        var city =  responseData.data.city;
      }else{
        var country = 'Other';
        var countryCode =  'Other';
        var city =  'Other';
      }

      url.clicks++;

      await url.save();

      const visitorInformation = new VisitorInformation({

          urlId: url._id,
          isAuthoritative: useragent.isAuthoritative,
          isMobile: useragent.isMobile,
          isMobileNative: useragent.isMobileNative,
          isTablet: useragent.isTablet,
          isiPad: useragent.isiPad,
          browser: useragent.browser,
          version: useragent.version,
          os: useragent.os,
          platform: useragent.platform,
          geoIp: clientIp,
          source: useragent.source,
          isBot: useragent.isBot,
          isMac: useragent.isMac,
          isChromeOS: useragent.isChromeOS,
          isDesktop: useragent.isDesktop,
          isWindows: useragent.isWindows,
          isLinux: useragent.isLinux,
          isLinux64: useragent.isLinux64,
          country,
          countryCode,
          city
      });

      await visitorInformation.save();

      // return res.status(200).json({
      //   status:"success",
      //   message:"get client details",
      //   data: "responseData"
      // });

	   return res.redirect(url.longUrl);
	
    } else {
      return res.status(404).json('No url found');
    }

  } catch (err) {
    console.error(err);
    res.status(500).json('Server error');
  }

});


router.get("/geo/location", async (req, res) => {


    const clientIp = requestIp.getClientIp(req);
    const staticIp = "2402:3a80:15f2:3397:741e:aedd:edeb:5627";
    let url = 'https://www.iplocate.io/api/lookup/'+clientIp;

    const responseData = await axios.get(url);
    console.log(responseData);

    res.json({
      message: "list data",
      data: responseData.data
    });

  // var apiCall = unirest("GET",
  //   "https://ip-geolocation-ipwhois-io.p.rapidapi.com/json/"
  // );
  // apiCall.headers({
  //   "x-rapidapi-host": "ip-geolocation-ipwhois-io.p.rapidapi.com",
  //   "x-rapidapi-key": "srclZqaa9imshAk9Xzz55u27oltLp1SqdiFjsnmva9PTpf2j3f"
  // });
  // apiCall.end(function(result) {
  //   if (res.error) throw new Error(result.error);
  //     console.log(result.body);
  //     res.send(result.body);
  // });

});



module.exports = router;







  
    