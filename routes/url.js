const express = require('express');
const router = express.Router();
const validUrl = require('valid-url');
const shortid = require('shortid');
// const config = require('config');
const baseURL = "localhost:5000"

const Url = require('../models/Url');

// @route     POST /api/url/shorten
// @desc      Create short URL

router.post('/shorten', async (req, res) => {
  const { longUrl } = req.body;
  const baseUrl = baseURL;

//   // Check base url
  if (!validUrl.isUri(baseUrl)) {
    return res.status(401).json('Invalid base url');
  }

//   // Create url code
  const urlCode = shortid.generate();

//   // Check long url
  if (validUrl.isUri(longUrl)) {
    try {
      console.log("Searching DB For", longUrl);
      let url = await Url.findOne({ longUrl });

      if (url) {
        console.log("URL Exist Not Creating Short Code");
        res.json(url);
      } else {
        const shortUrl = baseUrl + '/' + urlCode;
        console.log("Creating Short Code");

        url = new Url({
          longUrl,
          shortUrl,
          urlCode,
          date: new Date()
        });

        await url.save();
        console.log("New Short Code Generated");
        res.json(url);
      }
    } catch (err) {
      console.error(err);
      res.status(500).json('Server error');
    }
  } else {
    res.status(401).json('Invalid long url');
  }
});

module.exports = router;
